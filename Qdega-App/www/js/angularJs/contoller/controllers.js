angular.module('starter.controllers', ['ngCordova'])

.controller('SignInCtrl', function($scope, $http,WebService,LocalStore,$state, $ionicHistory,$ionicPopup, $rootScope, ServiceNavigation, $translate, $filter) {
	  $scope.serviceNavigation = ServiceNavigation;
	 
	  
	  
//	 if(LocalStore.getPrefrenses('accessToken') !='' && LocalStore.getPrefrenses('accessToken') != 'undefined' && LocalStore.getPrefrenses('accessToken') != undefined){
//		 $state.go('app.dashboard');
//      }else{
  //ionic.platform.fullScreen();
  // Start intent to open email prompt to send email
  $scope.registerNewAccount=function(){
  if(window.plugins && window.plugins.emailComposer) {
            window.plugins.emailComposer.showEmailComposerWithCallback(function(result) {
                console.log("Response -> " + result);
            },
            "Request an invite for the Beer Card", // Subject
            "Hello /n This is a request for create account",                      // Body
            ["test@example.com"],    // To
            null,                    // CC
            null,                    // BCC
            false,                   // isHTML
            null,                    // Attachments
            null);                   // Attachment Data
        }
  };
  
  $scope.data={};
  // This method is responsible for Merchant Sign in the aap
  $scope.signIn = function(data) {
  	if ( data.userEmail == 'undefined' || data.userEmail == undefined || data.userEmail == '' || data.userEmail == null) {
			WebService.showAlert('Please enter valid Email');
			data.password='';
		} else if (data.password == 'undefined' || data.password == '' || data.password == null) {
			WebService.showAlert('Please enter password');
			data.password='';
		} else {
//			 $ionicHistory.nextViewOptions({
//             historyRoot: true
//             });
			 var params={email :data.userEmail, password : data.password };
			  var url='http://mastersoftwaretechnologies.com/kolsch_card/api/login';
			  var headers={clientid:'hj78d17047d1e4d19019g7uy1412e569939b7y6t',secretkey:'up89d17047d1e4d19019hy4e1412e569939b8u7t'};
		      data.password='';
			  /*
			   *  method to hit server Api with GET and POST (url,parameters,requestMethod, Headers)
			   */
			  var result=WebService.makeServiceCall(url,params, 'POST',headers);
			  result.then(function(response) {
				  if(response.error==true){
						WebService.showAlert(''+response.message);
				  }else{
					  LocalStore.setPrefrenses('accessToken',response.access_token);
					  LocalStore.setPrefrenses('userEmailID',response.email);
					  LocalStore.setPrefrenses('FirstName',response.first_name);
					  LocalStore.setPrefrenses('LastName',response.last_name);
					  LocalStore.setPrefrenses('UserID',response.user_id);
					  $state.go('app.dashboard');
				  }
			    }, function(response) {
			      WebService.showAlert(''+response.message);
			      data.password='';
			    });
		}  
	//$ionicViewSwitcher.nextDirection("forward");
  };
//      }
})
.controller('ForgotPasswordCtrl', function($scope,$state, $stateParams,WebService,$ionicPopup) {
	  $scope.user={};
	 $scope.forgotPassword=function(data){
		 
		 var emailValid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
		  if( data.userEmail == '' || data.userEmail == null)  {
				  WebService.showAlert('Please enter valid Email');
			}else  if (!emailValid.test(data.userEmail)){
 				 WebService.showAlert('Please enter valid email');
 			}else  if(data.userEmail == 'undefined' || data.userEmail == undefined )  {
				 WebService.showAlert('Please enter Email');
			}else{
				  var url='http://mastersoftwaretechnologies.com/kolsch_card/api/forgotPassword/';
				  /*
				   *  method to hit server Api with GET and POST (url,parameters,requestMethod, Headers)
				   */
				  var result=WebService.makeServiceCall(url+data.userEmail,"", 'GET',"");
				  result.then(function(response) {
					  if(response.error==true){
						  WebService.showAlert(''+response.message);
					  }else{
						  WebService.showAlert(''+response.message);
						  $state.go('sign-in');
					  }
				    }, function(response) {
				      WebService.showAlert(''+response.message);
				    });
			}
	 }
	
	
})

//Account Controller
.controller('AccountCtrl', function($scope, $ionicPopup,$state,LocalStore,WebService, ServiceNavigation, $translate, $ionicHistory, $rootScope) {
   
    $scope.serviceNavigation = ServiceNavigation;
    $scope.languages = [];
    $scope.lang = "";
    
    
    for(lang in translations)
    {   
        var language = {};
        language.label = translations[lang].generic.languageName;
        language.value = lang;
        $scope.languages = $scope.languages.concat(language);
    }

    $scope.$on('$ionicView.beforeEnter', function(e) {
        $scope.currentLanguage = window.localStorage['qdega-language'];
    });
    
    $scope.changeLanguage = function(lang)
    {
        var oldLang = window.localStorage['qdega-language'];
        console.log("old lang " + oldLang);
        $translate.use(lang);
        console.log("new lang " + lang);
        window.localStorage['qdega-language'] = lang;
       
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        
        $ionicHistory.nextViewOptions({
			    disableBack: true
			  });
        
        $rootScope.qdegaNumberFormat = $translate.instant('generic.numberFormater');
        
        //$state.go($state.current, {}, {reload: true});
        $state.go('app.dashboard');
    };
    
    
    //Toggle
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
        } 
        else {
        	if(group=='changeInfo'){
        		  $scope.waiter={};
        		  $scope.waiter.firstName=LocalStore.getPrefrenses('FirstName');
        		  $scope.waiter.lastName=LocalStore.getPrefrenses('LastName');
        	    $scope.add=function(waiter){
//        	    	waiter = {};
        	    	
        	      	if ( waiter.firstName == 'undefined' || waiter.firstName == '' || waiter.firstName == null) {
        				WebService.showAlert('Please enter firstName');
        			} else if (waiter.lastName == 'undefined' || waiter.lastName == '' || waiter.lastName == null) {
        				WebService.showAlert('Please enter lastname');
        			} else {
        	    	
        	     var params={user_id :LocalStore.getPrefrenses('UserID'), first_name : waiter.firstName,last_name:waiter.lastName };
       			  var url='http://mastersoftwaretechnologies.com/kolsch_card/api/update';
       			  var headers={authorizationkey :LocalStore.getPrefrenses('accessToken')};
//       			var headers={authorizationkey :'01e658a2e06bdca8b0180269e3c18d941d3b97b2'};
       			  
       			  var result=WebService.makeServiceCall(url,params, 'POST',headers);
       			  result.then(function(response) {
       				  if(response.error==true){
       						WebService.showAlert(''+response.message);
       				  }else{
       					WebService.showAlert(''+response.message);
       				  }
       			    }, function(response) {
       			   WebService.showAlert(''+response.message);
       			    });
        	    }
        	   }
        	    
        	}else if(group=='changeEmail'){
        		 $scope.waiter={};
        		 $scope.add=function(waiter){
//        			 waiter = {};
        			 var emailValid = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        			 
        			 if ( waiter.email == 'undefined' || waiter.email == '' || waiter.email == null) {
         				WebService.showAlert('Please enter email');
         			}else  if (!emailValid.test(waiter.email)){
         				WebService.showAlert('Please enter valid email');
         				
         			} else if (waiter.confirmEmail == 'undefined' || waiter.confirmEmail == '' || waiter.confirmEmail == null) {
         				WebService.showAlert('Please confirm email');
         			} else if (waiter.email != waiter.confirmEmail ) {
         				WebService.showAlert('Email not confirmed');
         			} else if (waiter.emailCurrentPassword == 'undefined' || waiter.emailCurrentPassword == '' || waiter.emailCurrentPassword == null) {
         				WebService.showAlert('Please enter current password');
         			} else {
        			 
            	     var params={user_id :LocalStore.getPrefrenses('UserID'), email : waiter.email, password : waiter.emailCurrentPassword };
           			  var url='http://mastersoftwaretechnologies.com/kolsch_card/api/changeEmail';
           			  var headers={authorizationkey :LocalStore.getPrefrenses('accessToken')};
           			  
           			  var result=WebService.makeServiceCall(url,params, 'POST',headers);
           			  result.then(function(response) {
           				  if(response.error==true){
           						WebService.showAlert(''+response.message);
           				  }else{
           					WebService.showAlert(''+response.message);
           					$state.go('sign-in');
           				  }
           			    }, function(response) {
           				WebService.showAlert(''+response.message);
           			    });
            	    }
    			}
        	
        	}else if(group=='changePassword'){
        		 $scope.waiter={};
        		 $scope.add=function(waiter){
//        			 waiter = {};
        			 
        			 if ( waiter.currentPassword == 'undefined' || waiter.currentPassword == '' || waiter.currentPassword == null) {
          				WebService.showAlert('Please enter current password');
          			}else if (waiter.newPassword == 'undefined' || waiter.newPassword == '' || waiter.newPassword == null) {
          				WebService.showAlert('Please enter new password');
          			}else if ( waiter.newPassword.length < 6) {
          				WebService.showAlert('Password must contain 6 characters');
          			}else if (waiter.confirmNewPassword == 'undefined' || waiter.confirmNewPassword == '' || waiter.confirmNewPassword == null) {
          				WebService.showAlert('Please Confirm Password');
          			} else if (waiter.newPassword != waiter.confirmNewPassword ) {
          				WebService.showAlert('Password not confirmed');
          			}  else {
        			 
        			 
            	     var params={user_id :LocalStore.getPrefrenses('UserID'), oldpass : waiter.currentPassword,password : waiter.newPassword, confirm_pass:waiter.confirmNewPassword };
           			  var url='http://mastersoftwaretechnologies.com/kolsch_card/api/changePassword';
           			  var headers={authorizationkey :LocalStore.getPrefrenses('accessToken')};
           			  
           			  var result=WebService.makeServiceCall(url,params, 'POST',headers);
           			  result.then(function(response) {
           				  if(response.error==true){
           						WebService.showAlert(''+response.message);
           				  }else{
           					WebService.showAlert(''+response.message);
           				  }
           			    }, function(response) {
            			   WebService.showAlert(''+response.message);
           			    });
          			}
            	    }
        	}else if(group=='changePIN'){
       		 $scope.waiter={};
       		 $scope.add=function(waiter){
       			 
       			 if ( waiter.newPIN == 'undefined' || waiter.newPIN == '' || waiter.newPIN == null) {
         				WebService.showAlert('Please enter current password');
         			}else if (waiter.confirmNewPIN == 'undefined' || waiter.confirmNewPIN == '' || waiter.confirmNewPIN == null) {
         				WebService.showAlert('Please confirm pin');
         			}else if ( waiter.newPIN.length < 4) {
         				WebService.showAlert('pin must contain 4 characters');
         			}else if (waiter.pinCurrentPassword == 'undefined' || waiter.pinCurrentPassword == '' || waiter.pinCurrentPassword == null) {
         				WebService.showAlert('Please enter current password');
         			} else if (waiter.newPIN != waiter.confirmNewPIN ) {
         				WebService.showAlert('Pin not confirmed');
         			}  else {
       			 
       			 
           	     var params={user_id :LocalStore.getPrefrenses('UserID'), oldpass : waiter.currentPassword,password : waiter.newPassword, confirm_pass:waiter.confirmNewPassword };
          			  var url='http://mastersoftwaretechnologies.com/kolsch_card/api/updatePin';
          			  var headers={authorizationkey :LocalStore.getPrefrenses('accessToken')};
          			  
          			  var result=WebService.makeServiceCall(url,params, 'POST',headers);
          			  result.then(function(response) {
          				  if(response.error==true){
          						WebService.showAlert(''+response.message);
          				  }else{
          					WebService.showAlert(''+response.message);
          				  }
          			    }, function(response) {
           			   WebService.showAlert(''+response.message);
          			    });
         			}
           	    }
       	}
            $scope.shownGroup = group;
        }
    };
  
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };
  
    //window.localStorage['qdega-language'] = currentLang;
    
    //console.log("Current Language is " + currentLang);
    
    //$translateProvider.preferredLanguage(currentLang);
})


.controller('AppCtrl', function($scope, $state, $rootScope, $ionicHistory, $timeout, ServiceNavigation) {

    $scope.serviceNavigation = ServiceNavigation;
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
    $rootScope.$on('$ionicSideMenuClose', function (e, side) {
        if (side == 'left') {
            $timeout(function () {
                $ionicHistory.nextViewOptions({
                    historyRoot: false,
                    disableAnimate: false,
                    expire: null
                });
            }, 100);
        }
    });

})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})


.controller('DashboardCtrl', function($scope, $state, $ionicHistory, ServiceChargeCustomer) {
    $scope.serviceChargeCustomer = ServiceChargeCustomer;
    //$scope.serviceNavigation = ServiceNavigation;
    //$ionicScrollDelegate.resize();
    //console.log(document.height);
    //$scope.$broadcast('scroll.resize');
    $scope.$on('$ionicView.enter', function(e) {
        console.log("test");
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        /*$ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableBack: true,
                historyRoot: true
              });*/
    });
    
    $scope.$on('$ionicView.beforeEnter', function(e) {
       
        $scope.serviceChargeCustomer.productsSelected = [];
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();

    });
})



.controller('AboutUsCtrl', function($scope, $state, ServiceNavigation) {
    
    $scope.serviceNavigation = ServiceNavigation;
    
})

.controller('ChargeCustomer1Ctrl', function($scope, LocalStore,$state, ServiceNavigation,$cordovaBarcodeScanner) {
    
	   $scope.serviceNavigation = ServiceNavigation;
	   
	    
	    $cordovaBarcodeScanner.scan().then(function(imageData) {
	           alert(imageData.text);
	           
	           $scope.barcode=imageData.text;
	           
	           console.log("Barcode Format -> " + imageData.format);
	           console.log("Cancelled -> " + imageData.cancelled);
	       }, function(error) {
	           console.log("An error happened -> " + error);
	       });
	   
	   
	   $scope.step2 = function(){
		   
		
		   
	       $state.go('app.charge-customer-step2');
	   };
	   
	     $scope.scanBarcode = function() {
	     
	      
	   };
    
})

//ChargeCustomer2Ctrl Product Selection 
.controller('ChargeCustomer2Ctrl', function($scope, $state, $filter, $ionicHistory, $ionicPopup, ServiceChargeCustomer, ServiceNavigation, $translate) {
    
    $scope.serviceChargeCustomer = ServiceChargeCustomer;
    $scope.serviceNavigation = ServiceNavigation;
    
    $scope.productItem = {};
    $scope.productItem.id = 0;
    $scope.productItem.quantity = 1;
    
    
    $scope.productItemEdit = {};
    $scope.productItemEditIndex = 0;
    $scope.productItemEdit.id = 0;
    $scope.productItemEdit.quantity = 1;
    
    //subtract quantity from edit product
    $scope.subtractQuantityEdit = function() {
        if($scope.productItemEdit.quantity != 0)
        {   
            $scope.productItemEdit.quantity = $scope.productItemEdit.quantity - 1;
        }
    };
    
    //add quantity to edit product
    $scope.addQuantityEdit = function() {
        
        $scope.productItemEdit.quantity = $scope.productItemEdit.quantity + 1;
    };
    
    
    
    $scope.openModal = function($index) {
        
        $scope.productItemEditIndex = $index;
        $scope.productItemEdit.id = $scope.serviceChargeCustomer.productsSelected[$index].id;
        $scope.productItemEdit.quantity = $scope.serviceChargeCustomer.productsSelected[$index].quantity;
        //$scope.productItemEdit = $scope.serviceChargeCustomer.productsSelected[$index]; 
        //console.log($scope.productItemEdit.quantity);
        
        var myPopup = $ionicPopup.show({
            templateUrl : 'templates/charge-customer-step2-quantity-edit.html',
            title: $translate.instant('chargeCustomerStep2Page.editPopupTitle') + $scope.serviceChargeCustomer.getProductName($scope.productItemEdit.id),
            scope: $scope,
            buttons: [
              
              {
                text: $translate.instant('chargeCustomerStep2Page.editPopupBtnOk'),
                type: 'button-dark',
                onTap: function(e) {
                    if($scope.productItemEdit.quantity == 0)
                    {
                        $scope.serviceChargeCustomer.productsSelected.splice($scope.productItemEditIndex, 1);
                    }
                    else
                    {
                        $scope.serviceChargeCustomer.productsSelected[$scope.productItemEditIndex].quantity = $scope.productItemEdit.quantity;
                    }
                }
              },
                { 
                    text: $translate.instant('chargeCustomerStep2Page.editPopupBtnCancel'),
                    type: 'button-dark'
                }
            ]
        });
        myPopup.then(function(res) {
            console.log('Tapped!', res);
        });
    };
    
    $scope.$on('$ionicView.beforeEnter', function(e) {
        
        var view = $ionicHistory.backView();
        if(view != null && view.stateId == "app.dashboard")
        {
            $scope.productItem = {};
            $scope.productItem.id = 0;
            $scope.productItem.quantity = 1;
            $scope.serviceChargeCustomer.productsSelected = [];

            //angular.element(document.getElementById("totalAmount")).html("Mex$ " + $scope.serviceChargeCustomer.getTotalAmount());
        }
    });
    
    //add a new product row
    $scope.addProduct = function(productItem) {
        if(productItem.id != 0 && productItem.quantity != 0)
        {
            var product = {};
            product.id = productItem.id;
            product.quantity = productItem.quantity;
            $scope.serviceChargeCustomer.productsSelected = $scope.serviceChargeCustomer.productsSelected.concat(product);

            $scope.productItem.id = 0;
            $scope.productItem.quantity = 1;
        }
    };
    
    //subtract quantity for current product
    $scope.subtractQuantity = function() {
        if($scope.productItem.quantity != 0)
        {   
            $scope.productItem.quantity = $scope.productItem.quantity - 1;
        }
        
        //angular.element(document.getElementById("totalAmount")).html("Mex$ " + $scope.serviceChargeCustomer.getTotalAmount());
    };
    
    //add quantity to current product
    $scope.addQuantity = function() {
        
        $scope.productItem.quantity = $scope.productItem.quantity + 1;
        
        //angular.element(document.getElementById("totalAmount")).html("Mex$ " + $scope.serviceChargeCustomer.getTotalAmount());
    };
    
    //Go to next step
    $scope.step3 = function()
    {  
        $state.go('app.charge-customer-step3');
    };
    
    
})


//Start ChargeCustomer3Ctrl, Payment Confirmation
.controller('ChargeCustomer3Ctrl', function($scope, $state, $ionicPopup, $timeout, $ionicViewSwitcher, $ionicHistory, ServiceChargeCustomer, ServiceNavigation, $translate) {
	
    $scope.serviceChargeCustomer = ServiceChargeCustomer;
    $scope.serviceNavigation = ServiceNavigation;
    
    $scope.pin = {txt1 : '', txt2 : '', txt3 : '', txt4 : ''};
    
    //pin entered focus on next
    $scope.pinEntered = function($event, textFieldId)
    {
        if(ionic.Platform.isIOS() || ionic.Platform.isIPad())
        {
            if($event != null)
            {
                if($event.keyCode >= 48 && $event.keyCode <= 57)
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
        }
        else if(ionic.Platform.isAndroid() || ionic.Platform.isWebView())
        {
        
            if(textFieldId == "chargeCustomerpin2")
            {
                if($scope.pin.txt1 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "chargeCustomerpin3")
            {
                if($scope.pin.txt2 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "chargeCustomerpin4")
            {
                if($scope.pin.txt3 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "btnRecharge")
            {
                if($scope.pin.txt4 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            }
            
    };
 
    //recharge customer
	$scope.customerRecharge = function() {
	   	var alertPopup = $ionicPopup.alert({
	     	title: $translate.instant('chargeCustomerStep3Page.confirmDialogTitle'),
	     	template: $translate.instant('chargeCustomerStep3Page.confirmDialogMessage'),
			buttons: [
			    {
					text: '<b>' + $translate.instant('chargeCustomerStep3Page.confirmDialogBtnHome') + '</b>',
			        type: 'button-dark',
			        onTap: function(e) {
						$ionicHistory.nextViewOptions({
						    disableBack: true
						  });
						$state.go('app.dashboard');
                        
                        $scope.pin = {txt1 : '', txt2 : '', txt3 : '', txt4 : ''};
                        $scope.serviceChargeCustomer.resetSelectedProducts();
			        }
			    }
			]
	   	});
	};
})


.controller('AddCredit1Ctrl', function($scope,LocalStore, $state, ServiceNavigation) {
    
    $scope.serviceNavigation = ServiceNavigation;
    
    alert("Saved Access Token: "+ LocalStore.getPrefrenses('accessToken'));
    
    $scope.step2 = function(){
        $state.go('app.add-credit-step2');
    };
})

.controller('AddCredit2Ctrl', function($scope, $state, ServiceAddCredit, ServiceNavigation) {
    $scope.serviceAddCredit = ServiceAddCredit;
    $scope.serviceNavigation = ServiceNavigation;
})

.controller('AddCredit3Ctrl', function($scope, $state, $ionicPopup, $timeout, $ionicHistory, ServiceAddCredit, ServiceNavigation, $translate) {
	
    $scope.serviceAddCredit = ServiceAddCredit;
    $scope.serviceNavigation = ServiceNavigation;
    
    $scope.pin = {txt1 : '', txt2 : '', txt3 : '', txt4 : ''};
    
    //pin entered focus on next
    $scope.pinEntered = function($event, textFieldId)
    {
       if(ionic.Platform.isIOS() || ionic.Platform.isIPad())
        {
            if($event != null)
            {
                if($event.keyCode >= 48 && $event.keyCode <= 57)
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
        }
        else if(ionic.Platform.isAndroid() || ionic.Platform.isWebView())
        {
            if(textFieldId == "addCreditpin2")
            {
                if($scope.pin.txt1 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "addCreditpin3")
            {
                if($scope.pin.txt2 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "addCreditpin4")
            {
                if($scope.pin.txt3 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "btnRecharge")
            {
                if($scope.pin.txt4 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
        }
            
    };
    
    //Add Credit
	$scope.addCredit = function() {
	   	var alertPopup = $ionicPopup.alert({
	     	title: $translate.instant('addCreditStep3Page.confirmDialogTitle'),
	     	template: $translate.instant('generic.currencySymbol2') + ' ' + 
                        $scope.serviceAddCredit.amount + ' ' + $translate.instant('addCreditStep3Page.confirmDialogMessage'),
			buttons: [
			    {
					text: '<b>' + $translate.instant('addCreditStep3Page.confirmDialogBtnHome') + '</b>',
			        type: 'button-dark',
			        onTap: function(e) {
						$ionicHistory.nextViewOptions({
						    disableBack: true
						  });
						$state.go('app.dashboard');
                        
                        $scope.pin = {txt1 : '', txt2 : '', txt3 : '', txt4 : ''};
                        $scope.serviceAddCredit.amount = 10;
			        }
			    }
			]
	   	});
	   	alertPopup.then(function(res) {
	     	console.log('Thank you for not eating my delicious ice cream cone');
			//$ionicViewSwitcher.nextDirection("backward");
			$ionicHistory.nextViewOptions({
			    disableBack: true
			  });
			$state.go('app.dashboard');
	   	});
	};
})

.controller('SellNewCard1Ctrl', function($scope, $state, ServiceNavigation) {
    
    $scope.serviceNavigation = ServiceNavigation;
    
    $scope.step2 = function(){
        $state.go('app.sell-new-step2');
    };
})

.controller('SellNewCard2Ctrl', function($scope, $state, ServiceSellNewCard, ServiceNavigation) {
	
    $scope.serviceSellNewCard = ServiceSellNewCard;
    $scope.serviceNavigation = ServiceNavigation;
    
})

.controller('SellNewCard3Ctrl', function($scope, $state, $ionicPopup, $timeout, $ionicHistory, ServiceSellNewCard, ServiceNavigation, $translate) {
	
    $scope.serviceSellNewCard = ServiceSellNewCard;
    $scope.serviceNavigation = ServiceNavigation;
    
    $scope.pin = {txt1 : '', txt2 : '', txt3 : '', txt4 : ''};
    
    //pin entered focus on next
    $scope.pinEntered = function($event, textFieldId)
    {
        if(ionic.Platform.isIOS() || ionic.Platform.isIPad())
        {
            if($event != null)
            {
                if($event.keyCode >= 48 && $event.keyCode <= 57)
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
        }
        else if(ionic.Platform.isAndroid() || ionic.Platform.isWebView())
        {
            if(textFieldId == "sellNewpin2")
            {
                if($scope.pin.txt1 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "sellNewpin3")
            {
                if($scope.pin.txt2 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "sellNewpin4")
            {
                if($scope.pin.txt3 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
            else if(textFieldId == "btnRecharge")
            {
                if($scope.pin.txt4 != "")
                {
                    var element = document.getElementById(textFieldId);
                    if(element)
                        element.focus();
                }
            }
        }
            
    };
    
	$scope.sellCard = function() {
	   	var alertPopup = $ionicPopup.alert({
	     	title: $translate.instant('sellNewCardStep3Page.confirmDialogTitle'),
	     	template: $translate.instant('generic.currencySymbol2') + ' ' + 
                        $scope.serviceSellNewCard.amount + ' ' + $translate.instant('sellNewCardStep3Page.confirmDialogMessage'),
			buttons: [
			    {
					text: '<b>' + $translate.instant('sellNewCardStep3Page.confirmDialogBtnHome') + '</b>',
			        type: 'button-dark',
			        onTap: function(e) {
						$ionicHistory.nextViewOptions({
						    disableBack: true
						  });
						$state.go('app.dashboard');
                        
                        $scope.pin = {txt1 : '', txt2 : '', txt3 : '', txt4 : ''};
                        $scope.serviceSellNewCard.amount = 10;
			        }
			    }
			]
	   	});
	   	alertPopup.then(function(res) {
	     	console.log('Thank you for not eating my delicious ice cream cone');
			//$ionicViewSwitcher.nextDirection("backward");
			$ionicHistory.nextViewOptions({
			    disableBack: true
			  });
			$state.go('app.dashboard');
	   	});
	};
})




//Waiters Controller
.controller('ManageWaitersCtrl', function($scope, $state, ServiceNavigation) {
    
    $scope.serviceNavigation = ServiceNavigation;
    
    $scope.groups = [];
    for (var i=0; i<10; i++) {
        $scope.groups[i] = {
        name: i,
        items: []
    };
    for (var j=0; j<3; j++) {
        $scope.groups[i].items.push(i + '-' + j);
    }
  }
  
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
        } 
        else {
            $scope.shownGroup = group;
        }
    };
  
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };
  
})

//Transactions Controller
.controller('TransactionsCtrl', function($scope, $state, ServiceNavigation) {
    
    $scope.serviceNavigation = ServiceNavigation;
    
})



//Charge Customer Service
.service('ServiceChargeCustomer', function($filter) {
    this.products = [
        { name: 'Product 1', id: 1, price: 1.1 },
        { name: 'Product 2', id: 2, price: 2.1 },
        { name: 'Product 3', id: 3, price: 3.1 },
        { name: 'Product 4', id: 4, price: 4.1 },
        { name: 'Product 5', id: 5, price: 5.1 },
    ];
    
    this.productsSelected = [
    ];
    
    this.resetSelectedProducts = function()
    {
        this.productsSelected = [
            { id: 2, quantity: 1 }
        ];
    };
    
    
    this.getTotalAmount = function() {
        var totalAmount = 0;
        for(var i = 0; i < this.productsSelected.length; i++)
        {
            if(this.productsSelected[i].id != 0)
                totalAmount += $filter('filter')(this.products, {id: this.productsSelected[i].id})[0].price * this.productsSelected[i].quantity;
        }
        
        return $filter('number')(totalAmount, 2);
    };
    
    this.getProductName = function(productId) {
        return $filter('filter')(this.products, {id: productId})[0].name;
    };
    
    this.getProductPrice = function(productId) {
        return $filter('filter')(this.products, {id: productId})[0].price;
    };
    
    this.getProductQuantityPrice = function(productId, quantity) {
        return $filter('filter')(this.products, {id: productId})[0].price * quantity;
    };
    
})

.service('ServiceAddCredit', function($filter) {
    
    this.amount = 10;
    
})


.service('ServiceSellNewCard', function($filter) {
    
    this.amount = 10;
    
})

.service('ServiceNavigation', function($filter, $state, $ionicHistory){
    
    this.dashboard = function()
    {
        var view = $ionicHistory.currentView();
        if(view.stateId != "app.dashboard")
        {
            //$ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableBack: true
              });
            $state.go('app.dashboard');
        }
    };
});

//window.localStorage.setItem("profile", 'My Profile');

//LocalStore.setPrefrenses('dinesh','spartak');

//if(window.localStorage.getItem("profile")!==undefined){
//	  alert("shared preferences on "+window.localStorage.getItem("profile"));
//}else{
//	  alert("shared preferences off");
//}