var appM=angular.module('starter.NetworkUtils', ['ngCordova'])

appM.factory('WebService', function($http,$q,$ionicLoading,$ionicPopup,LocalStore,refreshToken) {
	  return {
	    makeServiceCall: function(url,params,apiMethod,header) {
			  if(navigator.connection.type == Connection.NONE) {
				  
				  var alertPopup = $ionicPopup.alert({
						title : 'Internet Disconnected',	
						template : 'The internet is disconnected on your device.'
					});
				  
//	    	        $ionicPopup.confirm({
//	    	            title: "Internet Disconnected",
//	    	            content: "The internet is disconnected on your device."
//	    	        })
	    	    }
		  else{
			  $ionicLoading.show({
			      template: 'Loading...'
			    });
			  var defer = $q.defer();
	          $http({
	              method: apiMethod,
	              url: url,
	              data: params,
	              headers: header
	          }).success(function(data, status, header, config) {
	        	  $ionicLoading.hide();
	        	  if(data.statusCode==200){ //Success
	        		  defer.resolve(data);
		        	  console.log(status+' Response Data: '+JSON.stringify(data));
	        	  }else if(data.statusCode=601){
	        		  refreshToken.validationAlert("Invalid credentials");
	        	  }else if(data.statusCode=602){
	        		  refreshToken.validationAlert("Invalid Details");
	        	  }else if(data.statusCode=603){
	        		  refreshToken.validationAlert("Detail is missing or Empty");
	        	  }else if(data.statusCode=605){
	        		  refreshToken.validationAlert("Failed to update the information.");
	        	  }else if(data.statusCode==606){
	        		  refreshToken.validationAlert("Email is already exist");
	        	  }else if(data.statusCode==607){ //token expire
	        		  
	        		  console.log('status code '+data.statusCode);
	        		  var result=refreshToken.refresh(url,params,apiMethod,header);
	        		  result.then(function(response) {
	        			  defer.resolve(response);
	        		    }, function(response) {
	        		    	defer.reject(response);
	        		    });
	        		  
	        	  }else if(data.statusCode=608){
	        		  refreshToken.validationAlert("Password Or Old pass is not correct");
	        	  }else if(data.statusCode=609){
	        		  refreshToken.validationAlert("password does not match");
	        	  }else{
	        		  refreshToken.validationAlert("Server Error");
	        	  }
	        	
	          }).error(function(data, status, header, config) {
	        	  $ionicLoading.hide();
	        	  console.log(status+' Error: '+JSON.stringify(data));
	        	  
	        	  if(data.statusCode==607){
	        		  var result=refreshToken.refresh(url,params,apiMethod,header);
	        		  result.then(function(response) {
	        			  defer.resolve(response);
	        		    }, function(response) {
	        		    	defer.reject(response);
	        		    });
	        	  }else{
	        		  defer.reject(data);
	        	  }
	        	  
	        	 
	          });
	          
        return defer.promise;
		  }
	    },
	    showAlert: function(message) {
	    	var alertPopup = $ionicPopup.alert({
				title : 'Prepaid APP',	
				template : ''+message
			});
	    },
	    connectionCheck: function() {
	    	    if(navigator.connection.type == Connection.NONE) {
	    	        $ionicPopup.confirm({
	    	            title: "Internet Disconnected",
	    	            content: "The internet is disconnected on your device."
	    	        })
	    	        .then(function(result) {
	    	            if(!result) {
	    	                ionic.Platform.exitApp();
	    	            }
	    	        });
	    	    }
	    },
	  };
	});





appM.service('refreshToken',function($http,$q,$ionicLoading,UpdateAPI,LocalStore,$ionicPopup){
	return{
		refresh:function(url,params,apiMethod,header){
		  var defer = $q.defer();
		
		$http({
            method: 'GET',
            url: 'http://mastersoftwaretechnologies.com/kolsch_card/api/refreshToken',
            data: '',
            headers: {clientid:'testclient' , secretkey:'testpass',access_token: LocalStore.getPrefrenses('accessToken')}
        }).success(function(data, status, header, config) {
      	  $ionicLoading.hide();
      	  
      	  if(data.statusCode==604){
      		LocalStore.setPrefrenses('accessToken',data.access_token);
      		var UpdatedResult=UpdateAPI.updateResult(url,params,apiMethod,header);
      		UpdatedResult.then(function(response) {
      			defer.resolve(response);
  		    }, function(response) {
  		       defer.reject(response);
  		    });
      	  }
        }).error(function(data, status, header, config) {
      	  $ionicLoading.hide();
      	  defer.reject(data);
        });
		 return defer.promise;
		
	},
	  validationAlert:function(message){
		var alertPopup = $ionicPopup.alert({
			title : 'Prepaid APP',	
			template : ''+message
		});
	}
	}
});

appM.service('UpdateAPI',function($http,$q,$ionicLoading,LocalStore,$ionicPopup){
	return{
		updateResult:function(url,params,apiMethod,header){
		header.authorizationkey=LocalStore.getPrefrenses('accessToken');
		
		var defer = $q.defer();
		$http({
            method: apiMethod,
            url: url,
            data: params,
            headers: header
        }).success(function(data, status, header, config) {
      	  $ionicLoading.hide();
      	defer.resolve(data);
      	  
        }).error(function(data, status, header, config) {
      	  $ionicLoading.hide();
      	  defer.reject(data);
        });
		 return defer.promise;
	},
	}
});


appM.factory('LocalStore', function() {
	  return {
	    setPrefrenses: function(key,value) {
		  window.localStorage.setItem(key, value);
	  },
	  getPrefrenses:function(key){
		  return window.localStorage.getItem(key);
	  },
	  setPrefrensesObj: function(key,data) {
		  alert("set prefe "+JSON.stringify(data));
		  window.localStorage.setItem(key, JSON.stringify(data));
	  },
	  getPrefrensesObj:function(key){
		  alert("get "+window.localStorage.getItem(key));
		  return JSON.parse(window.localStorage.getItem(key));
	  }
	};
});






