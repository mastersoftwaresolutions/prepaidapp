// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.NetworkUtils','pascalprecht.translate'])

    .run(function($ionicPlatform, $rootScope) {
        
        $rootScope.qdegaNumberFormat = 2;
        //$cordovaStatusbar.overlaysWebView(true);
    
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            
            if (window.cordova && window.cordova.plugins.Keyboard) {
              cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
              cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
              // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
                StatusBar.styleLightContent();
                //StatusBar.overlaysWebView(true);
            }
        });
    })

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider) {
	
    //$ionicConfigProvider.views.maxCache(0);
	//$ionicConfigProvider.backButton.previousTitleText(false);
	$ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);
    
    
    //Language Start
    var deviceLanguage = "";
    var navigatorLangaue = navigator.language;
    navigatorLangaue = navigatorLangaue.toLowerCase();
    
    //add languages to translator
    for(lang in translations){
		$translateProvider.translations(lang, translations[lang]);
        
        console.log(translations[lang].generic.languageName);
        
        if(deviceLanguage == "")
            if(lang == navigatorLangaue || navigatorLangaue.indexOf(lang) > -1 )
            {
                deviceLanguage = lang;
            }
	}
    
    //getting old saved language
    var currentLang = window.localStorage['qdega-language'];
    //alert("Current App Language Saved in secure cache is " + currentLang);
    //alert("Device Language is " + navigatorLangaue);
    //alert("Language to adjust now is " + deviceLanguage)
    if(currentLang == undefined)
    {
        if(deviceLanguage != "")
            currentLang = deviceLanguage;
        else
            currentLang = "en";
        
        window.localStorage['qdega-language'] = currentLang;
    }
    
    console.log("Current Language is " + currentLang);
    
    //set current language in translator
    $translateProvider.preferredLanguage(currentLang);
    //Language End

    //app state
	$stateProvider
	
    .state('sign-in', {
        cache: false,
      	url: '/sign-in',
      	templateUrl: 'templates/sign-in.html',
      	controller: 'SignInCtrl'
    })
	
    .state('forgot-password', {
        cache: false,
      	url: '/forgot-password',
      	templateUrl: 'templates/forgot-password.html',
      	controller: 'ForgotPasswordCtrl'
    })

    .state('app', {
        cache: false,
    	url: '/app',
    	abstract: true,
    	templateUrl: 'templates/menu.html',
    	controller: 'AppCtrl'
  	})
  
  	.state('app.dashboard', {
        cache: false,
    	url: '/dashboard',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/dashboard.html',
        		controller: 'DashboardCtrl'
      	  	}
    	}
  	})
	
  	.state('app.charge-customer-step1', {
    	url: '/charge-customer-step1',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/charge-customer-step1.html',
        		controller: 'ChargeCustomer1Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.charge-customer-step2', {
        cache: false,
    	url: '/charge-customer-step2',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/charge-customer-step2.html',
        		controller: 'ChargeCustomer2Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.charge-customer-step3', {
        cache: false,
    	url: '/charge-customer-step3',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/charge-customer-step3.html',
        		controller: 'ChargeCustomer3Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.add-credit-step1', {
        cache: false,
    	url: '/add-credit-step1',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/add-credit-step1.html',
        		controller: 'AddCredit1Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.add-credit-step2', {
        cache: false,
    	url: '/add-credit-step2',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/add-credit-step2.html',
        		controller: 'AddCredit2Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.add-credit-step3', {
        cache: false,
    	url: '/add-credit-step3',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/add-credit-step3.html',
        		controller: 'AddCredit3Ctrl'
      	  	}
    	}
  	})
	
	
  	.state('app.sell-new-step1', {
        cache: false,
    	url: '/sell-new-step1',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/sell-new-step1.html',
        		controller: 'SellNewCard1Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.sell-new-step2', {
        cache: false,
    	url: '/sell-new-step2',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/sell-new-step2.html',
        		controller: 'SellNewCard2Ctrl'
      	  	}
    	}
  	})
	
  	.state('app.sell-new-step3', {
        cache: false,
    	url: '/sell-new-step3',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/sell-new-step3.html',
        		controller: 'SellNewCard3Ctrl'
      	  	}
    	}
  	})
    
    .state('app.manage-waiters', {
        cache: false,
      	url: '/manage-waiters',
      	views: 
		{
        	'menuContent': 
			{
          	  	templateUrl: 'templates/manage-waiters.html',
                controller: 'ManageWaitersCtrl'
        	}
      	}
    })
    
    .state('app.transactions', {
        cache: false,
      	url: '/transactions',
      	views: 
		{
        	'menuContent': 
			{
          	  	templateUrl: 'templates/transactions.html',
                controller: 'TransactionsCtrl'
        	}
      	}
    })
    
    .state('app.my-account', {
        cache: false,
      	url: '/my-account',
      	views: 
		{
        	'menuContent': 
			{
          	  	templateUrl: 'templates/my-account.html',
                controller: 'AccountCtrl'
        	}
      	}
    })
    
    .state('app.about-us', {
        cache: false,
      	url: '/about-us',
      	views: 
		{
        	'menuContent': 
			{
          	  	templateUrl: 'templates/about-us.html',
                controller: 'AboutUsCtrl'
        	}
      	}
    })
    
    
  	.state('app.search', {
        cache: false,
   	 	url: '/search',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/search.html'
      	  	}
    	}
  	})

  
  	.state('app.playlists', {
        cache: false,
      	url: '/playlists',
      	views: 
		{
        	'menuContent': 
			{
          	  	templateUrl: 'templates/playlists.html',
         	   	controller: 'PlaylistsCtrl'
       	 	}
      	}
    })

  
  
  	.state('app.single', {
    	url: '/playlist/:playlistId',
    	views: 
		{
      	  	'menuContent': 
			{
        		templateUrl: 'templates/playlist.html',
				controller: 'PlaylistCtrl'
			}
    	}
  	});
  
  	// if none of the above states are matched, use this as the fallback
  	$urlRouterProvider.otherwise('/sign-in');
});
