var translations = {
	"en": {
        generic : {
            numberFormater: "3",
            currencySymbol : "(Mex$)",//used on shopping cart page 
            currencySymbol2 : "Mex$",
            languageName : "English"
        },
        loginPage : {
            heading : "Please enter your login details",
            inputEmailAddress : "E-Mail Address",
            inputPassword : "Password",
            forgotPassword : "I forgot my password",
            btnLogin : "Confirm",
            btnLoginSubText : "Proceed to Homescreen",
            btnSignup : "Register  Account",
            btnSignupSubText : "Apply for a new Account"
        },
        forgotPasswordPage : {
            title : "Forgot Password",
            heading : "Please enter your email",
            inputEmailAddress : "E-Mail Address",
            btnForgotPassword : "Reset Password",
            btnForgotPasswordSubText : "Your password reset instructions will be emailed to you"
        },
        sideMenu : {
            home : "Home",
            aboutus : "About Us",
            account : "Account",
            logout : "Logout"
        },
        dashboardPage : {
            btnChargeCustomer : "Charge Customer",
            btnChargeCustomerSubText : "Scan Customer's QR-Code",
            btnAddCredit : "Add Credit",
            btnAddCreditSubText : "Add Credit to Customer's Account",
            btnSellNewCard : "Sell new Card",
            btnSellNewCardSubText : "Sell a new Card to a Customer",
            btnManageWaiters : "Manage Waiters",
            btnManageWaitersSubText : "Manage Credentials of your Waiters",
            btnMyTransactions : "My Transactions",
            btnMyTransactionsSubText : "Check your Balance",
        },
        shoppingCartComponent : {
            product : "Product",
            quantity : "Quantity",
            singlePrice : "Single Price",
            totalPrice : "Total Price",
            totalAmount : "Total Amount",
            consumerPIN : "Consumer's PIN",
            merchantPIN : "Merchant's PIN"
        },
        chargeCustomerStep1Page : {
            title : "Charge Customer",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner goes here"
        },
        chargeCustomerStep2Page : {
            title : "Charge Customer",
            heading : "Products",
            productSelectBox : "Select Product",
            btnAddProduct : "Add Product",
            btnChargeCustomer : "Charge Customer",
            btnChargeCustomerSubText : "Confirm Selection",
            editPopupTitle : "Edit Quantity of ",
            editPopupBtnOk : "OK",
            editPopupBtnCancel : "Cancel",
        },
        chargeCustomerStep3Page : {
            title : "Charge Customer",
            heading : "Confirm Payment",
            btnConfirm : "Confirm",
            btnConfirmSubText : "Authorize Payment",
            confirmDialogTitle : "Success!",
            confirmDialogMessage : "The Payment has been accepted",
            confirmDialogBtnHome : "Return to Home Screen"
        },
        addCreditStep1Page : {
            title : "Add Credit",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner goes here"
        },
        addCreditStep2Page : {
            title : "Add Credit",
            heading : "Add Credit",
            amountLbl : "Amount:",
            btnAddCredit : "Add Credit",
            btnAddCreditSubText : "Confirm Selection"
        },
        addCreditStep3Page : {
            title : "Add Credit",
            heading : "Confirm Payment",
            item : "Re-Charge Credit",
            itemQuantity : "1",
            btnConfirm : "Confirm",
            btnConfirmSubText : "Authorize Payment",
            confirmDialogTitle : "Success!",
            confirmDialogMessage : "has been added to the prepaid card.",
            confirmDialogBtnHome : "Return to Home Screen"
        },
        sellNewCardStep1Page : {
            title : "Sell New Card",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner goes here"
        },
        sellNewCardStep2Page : {
            title : "Sell New Card",
            heading : "Add Credit",
            amountLbl : "Amount:",
            btnAddCredit : "Add Credit",
            btnAddCreditSubText : "Confirm Selection"
        },
        sellNewCardStep3Page : {
            title : "Sell New Card",
            heading : "Confirm Payment",
            item : "Re-Charge Credit",
            itemQuantity : "1",
            btnConfirm : "Confirm",
            btnConfirmSubText : "Authorize Payment",
            confirmDialogTitle : "Success!",
            confirmDialogMessage : "has been added to the new prepaid card! <br />Please tell the customer to activate the card on www.corona-card.com before he can use the new prepaid card.",
            confirmDialogBtnHome : "Return to Home Screen"
        },
        manageWaitersPage : {
            title : "Manage Waiters",
            heading : "Manage Waiters",
            addWaiterTitle : "Add Waiter",
            inputFirstName : "First Name",
            inputLastName : "Last Name",
            inputEmailAddress : "Waiter's E-Mail Address",
            inputAssignAdminRights : "Assign Admin Rights",
            btnSendInvitation : "Send Invitation",
            manageWaiterTabTitle : "Manage Waiters",
            manageWaiterBtnDelete : "Delete",
            manageWaiterBtnAssignAdminRights : "Assign Admin Rights"
            
        },
        myTransactionsPage : {
            title : "My Transactions",
            heading : "Your Transactions this Month:",
            transactionType : "Type",
            transactionTotal : "Total",
            revenue : "Revenue",
            cashPayments : "Cash Payments",
            currentBalance : "Current balance",
            transactionDetails : "Details",
            forMerchantSymbol : "For",
            detailsByWaiterLbl : "Details by Waiters:",
            cardsLbl : "Cards"
        },
        accountPage : {
            title : "My Account",
            heading : "My Account",
            editAccountTabTitle : "Edit Account",
            inputFirstName : "First Name",
            inputLastName : "Last Name",
            btnEditAccountInfo : "Update Account Info",
            changeEmailTabTitle : "Change Email Address",
            inputNewEmailAddress : "New E-Mail Address",
            inputConfirmNewEmailAddress : "Confirm New E-Mail Address",
            inputCurrentPassword : "Current Password",
            btnUpdateEmailInfo : "Update Email Info",
            editChangePasswordTabTitle : "Change Password",
            inputNewPassword : "New Password",
            inputConfirmNewPassword : "Confirm New Password",
            btnUpdatePassword : "Update Password",
            changePinTabTitle : "Change PIN",
            inputNewPIN : "New PIN",
            inputConfirmNewPIN : "Confirm New PIN",
            btnUpdatePIN : "Update PIN",
            languageTabTitle : "Change Language",
            btnChangeLanguage : "Change Language"
        },
        aboutusPage : {
            title : "About Us",
            content : "About Us goes here"
        }
	},
	"de": {
        generic : {
            numberFormater: "2",
            currencySymbol : "(€)",
            currencySymbol2 : "€",
            languageName : "Deutsch"
        },
        loginPage : {
            heading : "Um Ihre Schicht zu beginnen, melden Sie sich bitte an",
            inputEmailAddress : "E-Mail-Adresse",
            inputPassword : "Passwort",
            forgotPassword : "Passwort vergessen",
            btnLogin : "Einloggen",
            btnLoginSubText : "Zum Hauptmenu",
            btnSignup : "Neu registrieren",
            btnSignupSubText : "Anfrage für einen neuen Account"
        },
        forgotPasswordPage : {
            title : "Passwort vergessen",
            heading : "Bitte geben Sie Ihre E-Mail-Adresse ein",
            inputEmailAddress : "E-Mail-Adresse",
            btnForgotPassword : "Passwort anfordern",
            btnForgotPasswordSubText : "Eine E-Mail mit Ihrem Passwort wurde verschickt"
        },
        sideMenu : {
            home : "Startseite",
            aboutus : "Über uns",
            account : "Konto",
            logout : "Schicht beenden"
        },
        dashboardPage : {
            btnChargeCustomer : "Bezahlvorgang",
            btnChargeCustomerSubText : "QR-Code eines Gastes scannen",
            btnAddCredit : "Guthaben aufladen",
            btnAddCreditSubText : "Guthaben an einen Gast verkaufen",
            btnSellNewCard : "Neue Karte verkaufen",
            btnSellNewCardSubText : "Neue Karte an einen Gast verkaufen",
            btnManageWaiters : "Accountverwaltung",
            btnManageWaitersSubText : "Mitarbeiter und eigenen Account verwalten",
            btnMyTransactions : "Meine Transaktionen",
            btnMyTransactionsSubText : "Transaktionen und Abrechnung einsehen",
        },
        shoppingCartComponent : {
            product : "Produkt",
            quantity : "Menge",
            singlePrice : "Einzelpreis",
            totalPrice : "Gesamt",
            totalAmount : "Gesamtbetrag",
            consumerPIN : "PIN des Kunden",
            merchantPIN : "PIN des Kassierers"
        },
        chargeCustomerStep1Page : {
            title : "Bezahlvorgang",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner kommt hier hin"
        },
        chargeCustomerStep2Page : {
            title : "Bezahlvorgang",
            heading : "Produktauswahl",
            productSelectBox : "Produkt wählen",
            btnAddProduct : "Produkt hinzufügen",
            btnChargeCustomer : "Kassieren",
            btnChargeCustomerSubText : "Auswahl bestätigen",
            editPopupTitle : "Menge ändern von ",
            editPopupBtnOk : "OK",
            editPopupBtnCancel : "Abbr.",
        },
        chargeCustomerStep3Page : {
            title : "Bezahlvorgang",
            heading : "Zahlug bestätigen",
            btnConfirm : "Bestätigen",
            btnConfirmSubText : "Zahlung authorisieren",
            confirmDialogTitle : "Glückwunsch!",
            confirmDialogMessage : "Die Zahlung wurde akzeptiert",
            confirmDialogBtnHome : "Zur Startseite"
        },
        addCreditStep1Page : {
            title : "Aufladung",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner kommt hier hin"
        },
        addCreditStep2Page : {
            title : "Aufladung",
            heading : "Betrag hinzufügen",
            amountLbl : "Betrag:",
            btnAddCredit : "Hinzufügen",
            btnAddCreditSubText : "Auswahl bestätigen"
        },
        addCreditStep3Page : {
            title : "Aufladung",
            heading : "Zahlung bestätigen",
            item : "Kartenaufladung",
            itemQuantity : "1",
            btnConfirm : "Bestätigen",
            btnConfirmSubText : "Zahlung authorisieren",
            confirmDialogTitle : "Glückwunsch!",
            confirmDialogMessage : "wurden dem Konto hinzugefügt.",
            confirmDialogBtnHome : "Zur Startseite"
        },
        sellNewCardStep1Page : {
            title : "Neue Karte verkaufen",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner kommt hier hin"
        },
        sellNewCardStep2Page : {
            title : "Neue Karte verkaufen",
            heading : "Betrag hinzufügen",
            amountLbl : "Betrag:",
            btnAddCredit : "Betrag hinzufügen",
            btnAddCreditSubText : "Auswahl bestätigen"
        },
        sellNewCardStep3Page : {
            title : "Neue Karte verkaufen",
            heading : "Zahlung bestätigen",
            item : "Kartenaufladung",
            itemQuantity : "1",
            btnConfirm : "Bestätigen",
            btnConfirmSubText : "Zahlung authorisieren",
            confirmDialogTitle : "Glückwunsch!",
            confirmDialogMessage : "wurden dem Konto hinzugefügt.<br />Bitte weisen Sie Ihren Kunden darauf hin, dass er die Karte noch auf www.corona-card.com aktivieren muss, bevor er die neue Karte nutzen kann.",
            confirmDialogBtnHome : "Zur Startseite"
        },
        manageWaitersPage : {
            title : "Personal verwalten",
            heading : "Personal verwalten",
            addWaiterTitle : "Mitarbeiter hinzufügen",
            inputFirstName : "Vorname",
            inputLastName : "Nachname",
            inputEmailAddress : "E-Mail-Adresse des Mitarbeiters",
            inputAssignAdminRights : "Adminrechte zuweisen",
            btnSendInvitation : "Einladung senden",
            manageWaiterTabTitle : "Personal verwalten",
            manageWaiterBtnDelete : "Löschen",
            manageWaiterBtnAssignAdminRights : "Adminrechte zuweisen"
            
        },
        myTransactionsPage : {
            title : "Meine Transaktionen",
            heading : "Meine Transaktionen diesen Monat:",
            transactionType : "Typ",
            transactionTotal : "Gesamt",
            revenue : "Verkaufswert",
            cashPayments : "Bareinnahmen",
            currentBalance : "Gesamtsumme",
            transactionDetails : "Details",
            forMerchantSymbol : "Für",
            detailsByWaiterLbl : "Details je Mitarbeiter:",
            cardsLbl : "Karten"
        },
        accountPage : {
            title : "Mein Account",
            heading : "Mein Account",
            editAccountTabTitle : "Account bearbeiten",
            inputFirstName : "Vorname",
            inputLastName : "Nachname",
            btnEditAccountInfo : "Account aktualisieren",
            changeEmailTabTitle : "E-Mail-Adresse ändern",
            inputNewEmailAddress : "Neue E-Mail-Adresse",
            inputConfirmNewEmailAddress : "Neue E-Mail-Adresse bestätigen",
            inputCurrentPassword : "Aktuelles Passwort",
            btnUpdateEmailInfo : "E-Mail-Adresse aktualisieren",
            editChangePasswordTabTitle : "Passwort ändern",
            inputNewPassword : "Neues Passwort",
            inputConfirmNewPassword : "Neues Passwort bestätigen",
            btnUpdatePassword : "Passwort aktualisieren",
            changePinTabTitle : "PIN ändern",
            inputNewPIN : "Neuer PIN",
            inputConfirmNewPIN : "Neuen PIN bestätigen",
            btnUpdatePIN : "PIN aktualisieren",
            languageTabTitle : "Sprache ändern",
            btnChangeLanguage : "Sprache ändern"
        },
        aboutusPage : {
            title : "Über die App",
            content : "Über die App kommt dann hier hin"
        }
	},
    "es": {
        generic : {
            numberFormater: "2",
            currencySymbol : "(€)",
            currencySymbol2 : "€",
            languageName : "Espagnol"
        },
        loginPage : {
            heading : "Para empezar su turno, por favor iniciar sesión:",
            inputEmailAddress : "Dirección de correo electrónico",
            inputPassword : "Contraseña",
            forgotPassword : "Solicitud de contraseña",
            btnLogin : "Iniciar Sesión",
            btnLoginSubText : "Después de iniciar sesión, vaya al menú principal",
            btnSignup : "Registrarse",
            btnSignupSubText : "Solicitud para una nueva cuenta"
        },
        forgotPasswordPage : {
            title : "Solicitud de contraseña",
            heading : "Por favor, introduzca su dirección de correo electrónico:",
            inputEmailAddress : "Dirección de correo electrónico",
            btnForgotPassword : "Solicitud de contraseña",
            btnForgotPasswordSubText : "Se enviará un correo electrónico con tu contraseña."
        },
        sideMenu : {
            home : "Hogar",
            aboutus : "Acerca de la aplicación",
            account : "Configuración del perfil",
            logout : "Capa de acabado"
        },
        dashboardPage : {
            btnChargeCustomer : "Proceso de pago",
            btnChargeCustomerSubText : "Escanear el código QR ​​de un invitado",
            btnAddCredit : "Recarga de crédito",
            btnAddCreditSubText : "Vender activos a un invitado",
            btnSellNewCard : "Venta Nuevo Mapa",
            btnSellNewCardSubText : "Vender nueva tarjeta a un host",
            btnManageWaiters : "Gestión del Empleado",
            btnManageWaitersSubText : "Administrar el personal y el perfil",
            btnMyTransactions : "Mis transacciones",
            btnMyTransactionsSubText : "Ver transacciones y facturación",
        },
        shoppingCartComponent : {
            product : "Producto",
            quantity : "Cantidad",
            singlePrice : "Precio",
            totalPrice : "Total",
            totalAmount : "Cantidad total",
            consumerPIN : "El PIN del cliente",
            merchantPIN : "PIN del cajero"
        },
        chargeCustomerStep1Page : {
            title : "Bezahlvorgang",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner kommt hier hin"
        },
        chargeCustomerStep2Page : {
            title : "Proceso de pago",
            heading : "Selección de producto",
            productSelectBox : "Seleccionar producto",
            btnAddProduct : "Añadir Producto",
            btnChargeCustomer : "Junto a solicitud del código PIN",
            btnChargeCustomerSubText : "Confirme la selección",
            editPopupTitle : "Cambiar la cantidad: ",
            editPopupBtnOk : "OK",
            editPopupBtnCancel : "Abortar",
        },
        chargeCustomerStep3Page : {
            title : "Proceso de pago",
            heading : "Pago Confirmación",
            btnConfirm : "Confirmar",
            btnConfirmSubText : "Autorizar el pago",
            confirmDialogTitle : "¡Felicidades",
            confirmDialogMessage : "El pago ha sido aceptado.",
            confirmDialogBtnHome : "Para el contenido principal"
        },
        addCreditStep1Page : {
            title : "Add Credit",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner goes here"
        },
        addCreditStep2Page : {
            title : "Add Credit",
            heading : "Add Credit",
            amountLbl : "Amount:",
            btnAddCredit : "Add Credit",
            btnAddCreditSubText : "Confirm Selection"
        },
        addCreditStep3Page : {
            title : "Add Credit",
            heading : "Confirm Payment",
            item : "Re-Charge Credit",
            itemQuantity : "1",
            btnConfirm : "Confirm",
            btnConfirmSubText : "Authorize Payment",
            confirmDialogTitle : "Success!",
            confirmDialogMessage : "has been added to the prepaid card.",
            confirmDialogBtnHome : "Return to Home Screen"
        },
        sellNewCardStep1Page : {
            title : "Sell New Card",
            heading : "QR-Code Scanner",
            qrscanArea : "QR Scanner goes here"
        },
        sellNewCardStep2Page : {
            title : "Sell New Card",
            heading : "Add Credit",
            amountLbl : "Amount:",
            btnAddCredit : "Add Credit",
            btnAddCreditSubText : "Confirm Selection"
        },
        sellNewCardStep3Page : {
            title : "Sell New Card",
            heading : "Confirm Payment",
            item : "Re-Charge Credit",
            itemQuantity : "1",
            btnConfirm : "Confirm",
            btnConfirmSubText : "Authorize Payment",
            confirmDialogTitle : "Success!",
            confirmDialogMessage : "has been added to the new prepaid card!<br /><br />Please tell the customer to activate the card on <b>www.corona-card.com</b> before he can use the new prepaid card.",
            confirmDialogBtnHome : "Return to Home Screen"
        },
        manageWaitersPage : {
            title : "Manage Waiters",
            heading : "Manage Waiters",
            addWaiterTitle : "Add Waiter",
            inputFirstName : "First Name",
            inputLastName : "Last Name",
            inputEmailAddress : "Waiter's E-Mail Address",
            inputAssignAdminRights : "Assign Admin Rights",
            btnSendInvitation : "Send Invitation",
            manageWaiterTabTitle : "Manage Waiters",
            manageWaiterBtnDelete : "Delete",
            manageWaiterBtnAssignAdminRights : "Assign Admin Rights"
        },
        myTransactionsPage : {
            title : "My Transactions",
            heading : "Your Transactions this Month:",
            transactionType : "Type",
            transactionTotal : "Total",
            revenue : "Revenue",
            cashPayments : "Cash Payments",
            currentBalance : "Current balance",
            transactionDetails : "Details",
            forMerchantSymbol : "For",
            detailsByWaiterLbl : "Details by Waiters:",
            cardsLbl : "Cards"
        },
        accountPage : {
            title : "My Account",
            heading : "My Account",
            editAccountTabTitle : "Edit Account",
            inputFirstName : "First Name",
            inputLastName : "Last Name",
            btnEditAccountInfo : "Update Account Info",
            changeEmailTabTitle : "Change Email Address",
            inputNewEmailAddress : "New E-Mail Address",
            inputConfirmNewEmailAddress : "Confirm New E-Mail Address",
            inputCurrentPassword : "Current Password",
            btnUpdateEmailInfo : "Update Email Info",
            editChangePasswordTabTitle : "Change Password",
            inputNewPassword : "New Password",
            inputConfirmNewPassword : "Confirm New Password",
            btnUpdatePassword : "Update Password",
            changePinTabTitle : "Change PIN",
            inputNewPIN : "New PIN",
            inputConfirmNewPIN : "Confirm New PIN",
            btnUpdatePIN : "Update PIN",
            languageTabTitle : "Change Language",
            btnChangeLanguage : "Change Language"
        },
        aboutusPage : {
            title : "About Us",
            content : "About Us goes here"
        }
    }
};